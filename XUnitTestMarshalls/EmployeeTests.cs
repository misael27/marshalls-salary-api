﻿using MarshallsSalary.Core.DTO;
using MarshallsSalary.Core.Models;
using MarshallsSalary.Web;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace XUnitTestMarshalls
{
    public class EmployeeTests : IClassFixture<TestFixture<Startup>>
    {
        private HttpClient Client;

        public EmployeeTests(TestFixture<Startup> fixture)
        {
            Client = fixture.Client;
        }

        [Fact]
        public async Task TestDataDummy()
        {
            // Arrange
            var request = new
            {
                Url = "/api/Employee/DataDummy"
            };

            // Act
            var response = await Client.PostAsync(request.Url, null);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetEmployeeByName()
        {
            // Arrange
            var employeeName = "Alison";
            var employeeSurname = "Doe";

            var request = new
            {
                Url = "/api/Employee/Name" + "?EmployeeName=" + employeeName + "&EmployeeSurname=" + employeeSurname
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestPostEmployeeSalary()
        {
            string Name = "TestName" + ContentHelper.RandomStringNumber(1);
            string Surname = "TestSurname" + ContentHelper.RandomStringNumber(1);
            string IdentificationNumber = ContentHelper.RandomStringNumber(8);
            string EmployeeCode = ContentHelper.RandomString(8);
            DateTime BeginDate = ContentHelper.RandomDay();
            DateTime Birthday = ContentHelper.RandomDay();
            int rdn = int.Parse(ContentHelper.RandomStringNumber(1));
            int DivisionId = 1;
            int PositionId = 1;
            int OfficeId = 1;
            int grade = int.Parse(ContentHelper.RandomStringNumber(1));

            var salaries = new List<SalaryDTO>();
            salaries.Add(
                new SalaryDTO()
                {
                    BaseSalary = ContentHelper.RandomFloat(),
                    Commission = ContentHelper.RandomFloat(),
                    CompensatioBonus = ContentHelper.RandomFloat(),
                    Contributions = ContentHelper.RandomFloat(),
                    ProductionBonus = ContentHelper.RandomFloat()
                }
            );

            var Body = new EmployeeDTO()
            {
                EmployeeName = Name,
                EmployeeSurname = Surname,
                IdentificationNumber = IdentificationNumber,
                EmployeeCode = EmployeeCode,
                BeginDate = BeginDate,
                Birthday = Birthday,
                Division = new DivisionDTO() { DivisionId = DivisionId },
                Position = new PositionDTO() { PositionId = PositionId },
                Grade = grade,
                Office = new OfficeDTO() { OfficeId = OfficeId },
                Salaries = salaries
            };

            // Arrange
            var request = new
            {
                Url = "/api/Employee",
                Body = Body
            };

            // Act
            var response = await Client.PostAsync(request.Url, ContentHelper.GetStringContent(request.Body));
            //remove if success
            if (response.IsSuccessStatusCode)
            {
                var requestGet = new
                {
                    Url = "/api/Employee/Name" + "?EmployeeName=" + Body.EmployeeName + "&EmployeeSurname=" + Body.EmployeeSurname
                };

                var responseGet = await Client.GetAsync(requestGet.Url);
                var jsonString = await responseGet.Content.ReadAsStringAsync();
                var resultGetDTO = JsonConvert.DeserializeObject<EmployeeDTO>(jsonString);
                var newId = resultGetDTO.Salaries.ToArray()[0].Id;
                var responseDelete =  await Client.DeleteAsync("/api/Employee/" + newId);
                Console.WriteLine(responseDelete);
            }

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetEmployeeList()
        {
            // Arrange
            var request = new
            {
                Url = "/api/Employee"
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task TestGetSalaryAverage()
        {
            // Arrange
            var employeeCode = "0MB1JOQP";

            var request = new
            {
                Url = "/api/Employee/Average" + "?EmployeeCode=" + employeeCode
            };

            // Act
            var response = await Client.GetAsync(request.Url);

            // Assert
            response.EnsureSuccessStatusCode();
        }

    }
}
